#ifndef SONG_HPP
#define SONG_HPP

#include <QString>

class Audio
{
public:
    QString title;
    QString cover;
    QString artist;
    QString album;
    QString source;
};

#endif // SONG_HPP
