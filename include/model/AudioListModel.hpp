#ifndef AUDIOLISTMODEL_HPP
#define AUDIOLISTMODEL_HPP

#include "Audio.hpp"

#include <QAbstractListModel>
#include <QList>

class AudioListModel :
    public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role {
        TITLE,
        COVER,
        ARTIST,
        ALBUM,
        SOURCE
    };

private:
    QList<Audio> _audios;

public:
    explicit AudioListModel(QObject* parent = nullptr);

public:
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    inline int rowCount(const QModelIndex& parent = {}) const override { return _audios.size(); }
    inline QHash<int,QByteArray> roleNames() const override
    {
        return {
            { TITLE, "title" },
            { COVER, "cover" },
            { ARTIST, "artist" },
            { ALBUM, "album" },
            { SOURCE, "source" },
        };
    }

    Audio& at(int index) { return _audios[index]; }

    Q_INVOKABLE void add(const Audio& audio);
};

#endif // AUDIOLISTMODEL_HPP
