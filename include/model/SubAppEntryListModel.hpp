#ifndef APPLICATIONENTRYLISTMODEL_HPP
#define APPLICATIONENTRYLISTMODEL_HPP

#include "model/SubAppEntry.hpp"

#include <QAbstractListModel>

class SubAppEntryListModel :
        public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role {
        APP,
        DISPLAY_NAME,
        ICON
    };

private:
    QList<SubAppEntry> _entries;

public:
    explicit SubAppEntryListModel(QObject* parent = nullptr);

public:
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    inline int rowCount(const QModelIndex& parent = {}) const override { return _entries.size(); }
    inline QHash<int,QByteArray> roleNames() const override
    {
        return {
            { APP, "app" },
            { DISPLAY_NAME, "displayName" },
            { ICON, "icon" }
        };
    }

    void add(const SubAppEntry& entry);
};

#endif // APPLICATIONENTRYLISTMODEL_HPP
