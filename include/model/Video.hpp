#ifndef VIDEO_HPP
#define VIDEO_HPP

#include <QString>

struct Video
{
public:
    QString title;
    QString source;
};

#endif // VIDEO_HPP
