#ifndef APPICATIONENTRY_HPP
#define APPICATIONENTRY_HPP

#include <QString>



struct SubAppEntry
{
public:
    QString app;
    QString displayName;
    QString icon;



public:
    explicit SubAppEntry(QString app, QString displayName, QString icon) :
        app(app), displayName(displayName), icon(icon)  {}
};

#endif // APPICATIONENTRY_HPP
