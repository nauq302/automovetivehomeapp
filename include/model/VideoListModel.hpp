#ifndef MEDIALISTMODEL_HPP
#define MEDIALISTMODEL_HPP

#include "Video.hpp"

#include <QAbstractListModel>
#include <QList>


class VideoListModel :
        public QAbstractListModel
{
    Q_OBJECT

private:
    QList<Video> _videos;

public:
    explicit VideoListModel(QObject* parent = nullptr);
};

#endif // MEDIALISTMODEL_HPP
