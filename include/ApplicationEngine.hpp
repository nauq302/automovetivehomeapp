#ifndef APPLICATIONENGINE_HPP
#define APPLICATIONENGINE_HPP

#include "pch.hpp"

#include "controller/BaseController.hpp"
//#include <concepts>

#include <QQmlApplicationEngine>
#include <QList>
#include <QQmlContext>

class ApplicationEngine :
        public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString currentApp READ currentApp WRITE setCurrentApp NOTIFY currentAppChanged)
    Q_PROPERTY(QString currentScreen READ currentScreen NOTIFY currentScreenChanged)

    friend class HomeController;

private:
    QQmlApplicationEngine _qmlEngine;

    QString _currentApp;
    QHash<QString,BaseController*> _controllers;

public:
    explicit ApplicationEngine(QObject* parent = nullptr);
    ~ApplicationEngine() override;

public:
    QString currentApp() const { return _currentApp; }
    QString currentScreen() const { return _controllers[_currentApp]->qmlFile(); }

    template<typename C>
    void add() {
        auto* controller = new C(this);
        _controllers.insert(controller->appCode(), controller);
        _qmlEngine.rootContext()->setContextProperty(controller->appCode(), controller);
    }

public slots:
    void setCurrentApp(const QString& newApp)
    {
        _currentApp = newApp;
        emit currentScreenChanged();
    }

signals:
    void currentAppChanged(const QString& newApp);
    void currentScreenChanged();


};

#endif // APPLICATIONENGINE_HPP
