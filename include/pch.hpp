#ifndef PCH_HPP
#define PCH_HPP

#include <QMetaType>
#include <QObject>
#include <QGuiApplication>
#include <QStringListModel>
#include <QString>
#include <QQmlApplicationEngine>
#include <QList>
#include <QQmlContext>
#include <QLocale>
#include <QTranslator>

#endif // PCH_HPP
