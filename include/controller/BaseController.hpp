#ifndef BASECONTROLLER_HPP
#define BASECONTROLLER_HPP

#include <QMetaType>
#include <QObject>

class ApplicationEngine;

class BaseController :
        public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString qmlFile READ qmlFile)

protected:
    QString _appCode;
    QString _displayName;
    QString _icon;
    QString _qmlFile;
    QString _widgetQmlFile;

public:
    explicit BaseController(QObject* parent = nullptr) : QObject(parent) {}

    explicit BaseController(
        const QString& appCode,
        const QString& displayName,
        const QString& icon,
        const QString& qmlFile,
        const QString& widgetQmlFile = "",
        QObject* parent = nullptr
    ) :
        QObject(parent),
        _appCode(appCode),
        _displayName(displayName),
        _icon(icon),
        _qmlFile(qmlFile),
        _widgetQmlFile(widgetQmlFile)
    {}

public:
    virtual const QString& appCode() const { return _appCode; }
    virtual const QString& displayName() const { return _displayName; }
    virtual const QString& icon() const { return _icon; }
    virtual const QString& qmlFile() const { return _qmlFile; }
    virtual const QString& widgetQmlFile() const { return _widgetQmlFile; }
};


#endif // BASECONTROLLER_HPP
