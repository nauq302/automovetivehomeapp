#ifndef MEDIACONTROLLER_HPP
#define MEDIACONTROLLER_HPP

#include "BaseController.hpp"
#include "model/AudioListModel.hpp"

#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QStandardPaths>


class MediaController :
        public BaseController
{
    Q_OBJECT

    Q_PROPERTY(AudioListModel* model READ model CONSTANT)
    Q_PROPERTY(QMediaPlayer* player READ player CONSTANT)

    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(bool repeating READ repeating WRITE setRepeating NOTIFY repeatingChanged)
    Q_PROPERTY(bool shuffling READ shuffling WRITE setShuffling NOTIFY shufflingChanged)
    Q_PROPERTY(bool playing READ playing NOTIFY playingChanged)
    Q_PROPERTY(QString currentTitle READ currentTitle NOTIFY currentTitleChanged)
    Q_PROPERTY(QString currentCover READ currentCover NOTIFY currentCoverChanged)
private:
    AudioListModel* _model;
    QMediaPlayer* _player;
    QMediaPlaylist* _playlist;

    QString _defaultCover;
    Audio* _currentAudio;
    bool _playing;
    bool _repeating;
    bool _shuffling;


public:
    explicit MediaController(QObject* parent = nullptr);



public:
    inline AudioListModel* model() const { return _model; }
    inline QMediaPlayer* player() const { return _player; }
    inline int currentIndex() const { return _playlist->currentIndex(); }
    inline bool playing() const { return _playing; }
    inline bool repeating() const { return _repeating; }
    inline bool shuffling() const { return _shuffling; }

    Q_INVOKABLE inline void play() { _playing = true; _player->play(); emit playingChanged(_playing); }
    Q_INVOKABLE inline void pause() { _playing = false; _player->pause(); emit playingChanged(_playing); }
    Q_INVOKABLE inline void next() { _playlist->next(); emit currentIndexChanged(currentIndex()); }
    Q_INVOKABLE inline void prev() { _playlist->previous(); emit currentIndexChanged(currentIndex());   }


public:
    inline void setCurrentIndex(int index) { _playlist->setCurrentIndex(index); _currentAudio = &_model->at(currentIndex()); }
    void setRepeating(bool repeating);
    void setShuffling(bool shuffling);

    QString currentTitle() const {
        return currentIndex() > 0 ? _model->at(currentIndex()).title : "";
    }

    QString currentCover() const
    {
        return currentIndex() > 0 ? _model->at(currentIndex()).cover : _defaultCover;
    }

    Q_INVOKABLE QString currentArtist() const { return _model->at(currentIndex()).artist; }
    Q_INVOKABLE QString currentAlbum() const { return _model->at(currentIndex()).album; }


signals:
    void currentIndexChanged(int index);
    void repeatingChanged(bool repeating);
    void shufflingChanged(bool shuffling);
    void playingChanged(bool playing);

    void currentTitleChanged();
    void currentCoverChanged();

private:
    void loadFrom(const QString& location);
    void add(const QUrl& url);
    QString coverOf(const QUrl& url);




};

#endif // MEDIACONTROLLER_HPP
