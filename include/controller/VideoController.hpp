#ifndef VIDEOCONTROLLER_HPP
#define VIDEOCONTROLLER_HPP

#include "BaseController.hpp"
#include "model/VideoListModel.hpp"
#include <QObject>

class VideoController : public BaseController
{
    Q_OBJECT

private:
    VideoListModel* model;

public:
    explicit VideoController(QObject *parent = nullptr);
};

#endif // VIDEOCONTROLLER_HPP
