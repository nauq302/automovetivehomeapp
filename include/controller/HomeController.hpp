#ifndef HOMECONTROLLER_HPP
#define HOMECONTROLLER_HPP

#include "BaseController.hpp"
#include "model/SubAppEntryListModel.hpp"
#include "ApplicationEngine.hpp"

#include <QStringListModel>


class HomeController :
        public BaseController
{
    Q_OBJECT

    Q_PROPERTY(SubAppEntryListModel* listModel MEMBER _listModel CONSTANT)
    Q_PROPERTY(QStringListModel* widgetModel MEMBER _widgetModel CONSTANT)

public:
    SubAppEntryListModel* _listModel;
    QStringListModel* _widgetModel;

public:
    explicit HomeController(QObject* parent = nullptr);

public:
    void load();

private:
    ApplicationEngine* getEngine() { return static_cast<ApplicationEngine*>(parent()); }

};

#endif // HOMECONTROLLER_HPP
