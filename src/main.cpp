    #include <QGuiApplication>


    #include <QLocale>
    #include <QTranslator>

#include "ApplicationEngine.hpp"

int main(int argc, char* argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

//    QTranslator translator;
//    QStringList uiLanguages = QLocale::system().uiLanguages();

//    for (auto& locale : uiLanguages) {
//        QString baseName = "AutomotiveHomeApplication_" + QLocale(locale).name();
//        if (translator.load(":/i18n/" + baseName)) {
//            app.installTranslator(&translator);
//            break;
//        }
//    }

    ApplicationEngine appEngine(&app);

    return app.exec();
}
