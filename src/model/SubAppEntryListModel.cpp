#include "model/SubAppEntryListModel.hpp"

SubAppEntryListModel::SubAppEntryListModel(QObject* parent) :
    QAbstractListModel(parent)
{

}

QVariant SubAppEntryListModel::data(const QModelIndex& index, int role) const
{
    int row = index.row();
    if (row < 0 || row >= _entries.size())
        return {};

    const SubAppEntry& entry = _entries[row];

    switch (role) {
        case APP: return entry.app;
        case DISPLAY_NAME: return entry.displayName;
        case ICON: return entry.icon;

        default: return {};
    }
}

void SubAppEntryListModel::add(const SubAppEntry& entry)
{
   beginInsertRows({}, rowCount(), rowCount());
   _entries.append(entry);
   endInsertRows();
}
