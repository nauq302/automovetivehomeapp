#include "include/model/AudioListModel.hpp"

AudioListModel::AudioListModel(QObject* parent) :
    QAbstractListModel(parent)
{

}

QVariant AudioListModel::data(const QModelIndex& index, int role) const
{
    int row = index.row();
    if (row < 0 || row >= _audios.size())
        return {};

    const Audio& audio = _audios[row];

    switch (role) {
        case TITLE: return audio.title;
        case COVER: return audio.cover;
        case ARTIST: return audio.artist;
        case ALBUM: return audio.album;
        case SOURCE: return audio.source;
        default: return {};
    }
}

void AudioListModel::add(const Audio& audio)
{
    beginInsertRows({}, rowCount(), rowCount());
    _audios.append(audio);
    endInsertRows();
}


