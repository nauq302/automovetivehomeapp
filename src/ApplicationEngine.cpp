#include "ApplicationEngine.hpp"
#include "controller/HomeController.hpp"
#include "controller/MediaController.hpp"
#include "controller/VideoController.hpp"
#include <QGuiApplication>



ApplicationEngine::ApplicationEngine(QObject* parent) :
    QObject(parent),
    _currentApp("home")
{
//    _controllers.insert({
//        { "home", new HomeController(this) },
//        { "media", new MediaController(this) },
//        { "video", new VideoController(this) }
//    });


    connect(this, &ApplicationEngine::currentAppChanged, this, &ApplicationEngine::setCurrentApp);


    _qmlEngine.rootContext()->setContextProperty("engine", this);
    add<HomeController>();
    add<MediaController>();
    add<VideoController>();
//    for (const auto& k : _controllers.keys()) {
//        _qmlEngine.rootContext()->setContextProperty(k, _controllers[k]);
//    }

    ((HomeController*)_controllers["home"])->load();


    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&_qmlEngine, &QQmlApplicationEngine::objectCreated, parent,
        [url](QObject* obj, const QUrl& objUrl) {
            if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    _qmlEngine.load(url);
}

ApplicationEngine::~ApplicationEngine()
{

}
