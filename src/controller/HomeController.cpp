#include "controller/HomeController.hpp"

HomeController::HomeController(QObject* parent) :
    BaseController("home", "", "", "homeScreen.qml", "", parent),
    _listModel(new SubAppEntryListModel(this)),
    _widgetModel(new QStringListModel(this))
{
    QStringList widgets;
    widgets << "qrc:/qml/components/media/MediaWidget.qml";
    _widgetModel->setStringList(widgets);



//    SubAppEntry media("media", "Media", "btn_home_menu_media_d.png");
//    SubAppEntry video("video", "Video", "btn_home_menu_climate_d.png");

//    _listModel->add(media);
    //    _listModel->add(video);
}

void HomeController::load()
{
    for (auto& c : static_cast<ApplicationEngine*>(parent())->_controllers) {
        if (c->appCode() != "home") {
            SubAppEntry entry(c->appCode(), c->displayName(), c->icon());
            _listModel->add(entry);
        }
    }
}
