#include "controller/MediaController.hpp"

#include <taglib/tag.h>
#include <taglib/fileref.h>
#include <taglib/id3v2tag.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2frame.h>
#include <taglib/id3v2header.h>
#include <taglib/attachedpictureframe.h>

#include <QDir>

#include <fstream>

MediaController::MediaController(QObject* parent) :
    BaseController("media", "Media", "btn_home_menu_media_d.png", "mediaScreen.qml", "", parent),
    _model(new AudioListModel(this)),
    _player(new QMediaPlayer(this)),
    _playlist(new QMediaPlaylist(this)),
    _defaultCover("qrc:///res/img/Media/backgrounds/bg_album5.png")
{
    _player->setPlaylist(_playlist);

    _playlist->setPlaybackMode(QMediaPlaylist::Loop);
    connect(_playlist, &QMediaPlaylist::currentIndexChanged, this, &MediaController::currentIndexChanged);

    connect(_player, &QMediaPlayer::mediaStatusChanged, [this](QMediaPlayer::MediaStatus status) {
        if (status != QMediaPlayer::EndOfMedia && _player->state() == QMediaPlayer::PlayingState) {
            play();
        }
    });

    loadFrom(QStandardPaths::standardLocations(QStandardPaths::StandardLocation::MusicLocation).at(0));

    if (_model->rowCount() != 0)
        setCurrentIndex(0);

}



void MediaController::setRepeating(bool repeating)
{
    if (_repeating == repeating)
        return;

    _repeating = repeating;

    QMediaPlaylist::PlaybackMode mode;

    if (repeating)
        mode = QMediaPlaylist::CurrentItemInLoop;
    else
        mode = _shuffling ? QMediaPlaylist::Random : QMediaPlaylist::Loop;

    _playlist->setPlaybackMode(mode);

    emit repeatingChanged(repeating);
}

void MediaController::setShuffling(bool shuffling)
{
    if (_shuffling == shuffling)
        return;

    _shuffling = shuffling;

    if (!_repeating)
        _playlist->setPlaybackMode(_shuffling ? QMediaPlaylist::Random : QMediaPlaylist::Sequential);

    emit shufflingChanged(shuffling);
}

void MediaController::loadFrom(const QString& location)
{
    QStringList nameFilters("*.mp3");
    QFileInfoList musics = QDir(location).entryInfoList(nameFilters, QDir::Files);
    QList<QUrl> urls;

    for (auto& m : musics) {
        urls.append(QUrl::fromLocalFile(m.absoluteFilePath()));
        add(urls.back());
    }
}

void MediaController::add(const QUrl& url)
{
    _playlist->addMedia(url);

    TagLib::FileRef f(url.path().toStdString().c_str());
    TagLib::Tag* tag = f.tag();

    _model->add({
        tag->title().toCString(), /// title
        coverOf(url), /// cover
        tag->artist().toCString(), /// artist
        tag->album().toCString(), /// album
        url.toDisplayString() /// source
    });
}

QString MediaController::coverOf(const QUrl& url)
{
    TagLib::MPEG::File mpeg(url.path().toStdString().c_str());
    TagLib::ID3v2::FrameList frameList = mpeg.ID3v2Tag()->frameList("APIC");

    if (frameList.isEmpty()) {
        qDebug() << "ID3v2 not present";
        return _defaultCover;
    }

    QString fileName = url.fileName() + ".jpg";
    std::ofstream jpeg(fileName.toStdString(), std::ios::binary);

    if (!jpeg.is_open()) {
        qDebug() << "Failed to create file " << fileName;
        return _defaultCover;
    }

    auto* frame = static_cast<TagLib::ID3v2::AttachedPictureFrame*>(frameList.front());
    TagLib::ByteVector picture = frame->picture();
    jpeg.write(picture.data(), picture.size());

    return QUrl::fromLocalFile(fileName).toDisplayString();
}


