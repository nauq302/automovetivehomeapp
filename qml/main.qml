import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: window
    width: 1500
    height: 864
    visible: true
    title: qsTr('Home Application')

    background: Image {
        source: "qrc:///res/img/bg_full.png"
    }


    Loader {
        id: loader
        width: parent.width
        height: parent.height
        source: "screens/" + engine.currentScreen
    }


}
