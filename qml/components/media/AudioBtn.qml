import QtQuick 2.0

Item {
    id: root

    width: 100
    height: 100

    property alias icon: ico.source
    signal clicked()

    Image {
        id: ico
        anchors.centerIn: parent
    }

    Image {
        id: background
        anchors.fill: parent
        opacity: 0.8
        source: "qrc:/res/img/Media/backgrounds/media_btn_f.png"
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            root.clicked()
        }

        onPressed: {
            background.source = "qrc:/res/img/Media/backgrounds/media_btn_p.png"
        }

        onReleased: {
            background.source = "qrc:/res/img/Media/backgrounds/media_btn_f.png"
        }

        onEntered: {
            background.opacity = 1.0
        }

        onExited: {
            background.opacity = 0.8
        }
    }
}
