import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15

Item {

    Image {
        id: background
        anchors.fill: parent
        source: media.currentCover
        opacity: 0
    }

    FastBlur {
        anchors.fill: background
        opacity: 0.5

        source: background
        radius: 32
    }

    Text {
        id: title
        color: 'white'
        text: media.currentTitle
        font.pointSize: 30

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 20
        }
    }


    Image {
        id: name
        anchors.centerIn: parent
        width: parent.width / 2
        height: parent.height / 2

        source: media.currentCover
    }


    ProgressBar {
        from: 0
        value: media.player.position
        to: media.player.duration
        anchors {
            bottom: parent.bottom
            bottomMargin: 32
            horizontalCenter: parent.horizontalCenter
        }
        width: parent.width * 0.9
    }


    function goToMedia() {
        engine.currentApp = 'media'
    }


}
