import QtQuick 2.0




Item {
    property alias currentIndex: pvSong.currentIndex

    Component {
        id: appDelegate
        Item {
            id: wrapper
            width: 400
            height: 400
            scale: PathView.iconScale === undefined ? 1 : PathView.iconScale
            z: PathView.isCurrentItem ? 1 : 0
            opacity: PathView.isCurrentItem ? 1 : 0.25
            Image {
                id: myIcon
                y: 20
                anchors.horizontalCenter: parent.horizontalCenter
                source: cover
                width: 400
                height: 400
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    media.currentIndex = index
                }
            }
        }
    }

    PathView {
        id: pvSong
        anchors.fill: parent
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        focus: true
        model: media.model
        interactive: true
        pathItemCount: 3
        maximumFlickVelocity : 150
        delegate: appDelegate

        path: Path {
            startX: 185
            startY: 150
            PathAttribute { name: "iconScale"; value: 0.35 }
            PathQuad {
                x: 585
                y: 150
                controlX: 260
                controlY: 150
            }
            PathAttribute { name: "iconScale"; value: 1 }
            PathQuad {
                x: 896
                y: 150
                controlX: 750
                controlY: 150
            }
            PathAttribute { name: "iconScale"; value: 0.25 } //0.15
        }


    }


}

