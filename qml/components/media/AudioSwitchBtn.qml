import QtQuick 2.0

AudioBtn {
    id: root

    property bool on: false

    property url onIcon: ''
    property url offIcon: ''

    signal switched()

    icon: on ? onIcon : offIcon

    onClicked: {
        on = !on
        root.switched()
    }
}
