import QtQuick 2.0

Item {
    id: widget
    property alias widget: loader.source
    property string appCode


    Image {
        id: background
        source: "qrc:///res/img/HomeScreen/bg_widget_f.png"
        width: parent.width
        height: parent.height
    }

    Loader {
        id: loader
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent

        onClicked: engine.currentApp = appCode
        onPressed: background.source = "qrc:///res/img/HomeScreen/bg_widget_p.png"
        onExited: background.source = "qrc:///res/img/HomeScreen/bg_widget_f.png"

    }

}
