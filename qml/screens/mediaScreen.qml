import QtQuick 2.15
import QtQuick.Controls 2.15

import '../components/media'

Item {
    width: parent.width
    height: parent.height

    Item {
        id: titleBar
        height: 100

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }



        AudioBtn {
            icon: "qrc:///res/img/StatusBar/btn_top_back_n.png"
            onClicked: engine.currentApp = 'home'
        }

        Text {
            text: 'USB Music'
            anchors.centerIn: parent
            color: 'white'
            font.pointSize: 32

        }

        Image {
            id: line
            source: "qrc:///res/img/Media/backgrounds/line_title.png"
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
        }
    }

    Item {
        id: audioListWrapper

        width: 512

        anchors {
            top: titleBar.bottom
            left: parent.left
            bottom: parent.bottom
        }

        clip: true

        ListView {
            id: audioList
            anchors.fill: parent
            model: media.model
            currentIndex: media.currentIndex

            delegate: Item {
                property var xmodel: model
                width: parent.width - 24
                height: 128
                clip: true

                Text {
                    text: title
                    color: 'white'
                    font.pointSize: 24
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: 64
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: media.currentIndex = index
                }
            }

            highlight: Image {
                source: "qrc:///res/img/Media/backgrounds/popup_list_f.png"
                width: parent.width
                height: 128

                Image {
                    source: "qrc:///res/img/Media/icons/ico_play.png"
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: 20
                    }
                }
            }


        }


        Image {
            id: audioListBackgrond
            source: "qrc:///res/img/Media/backgrounds/bg_frame.png"
            anchors.fill: parent
        }
    }



    Item {
        id: audioViewer

        anchors {
            top: titleBar.bottom
            left: audioListWrapper.right
            right: parent.right
            bottom: parent.bottom
        }

        Item {
            id: topbarWrapper

            height: 128

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Text {
                id: audioTitle
                text: audioList.currentItem.xmodel.title
                color: 'white'
                font.pointSize: 30
                anchors.top: parent.top
            }

            Text {
                id: audioArtist
                text: audioList.currentItem.xmodel.artist
                color: 'white'
                font.pointSize: 28
                anchors.top: audioTitle.bottom
            }

            Text {
                id: audioAlbum
                text: audioList.currentItem.xmodel.album
                color: 'white'
                font.pointSize: 24
                anchors.top: audioArtist.bottom

            }

            Image {
                source: "qrc:///res/img/Media/icons/list_ico_song_n.png"
                anchors {
                    right: audiosCount.left
                    verticalCenter: audiosCount.verticalCenter
                }
            }

            Text {
                id: audiosCount
                text: audioList.count
                color: 'white'
                font.pointSize: 24
                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: 20
                }
            }
        }

        AudioPathView {
            id: pathView
            anchors {
                top: topbarWrapper.bottom
                left: parent.left
                right: parent.right
                bottom: controlBarWrapper.top
            }
        }

        Item {
            id: controlBarWrapper
            height: 200

            anchors {
                left: parent.left
                right: parent.right
                bottom: progressBarWrapper.top
            }



            Row {
                anchors.centerIn: parent

                AudioSwitchBtn {
                    id: shuffleBtn
                    onIcon: "qrc:///res/img/Media/icons/ico_shuffle_all_p.png"
                    offIcon: "qrc:///res/img/Media/icons/ico_shuffle_all_d.png"

                    onSwitched: media.shuffling = on
                }

                AudioBtn {
                    id: prevBtn
                    icon: "qrc:///res/img/Media/icons/media_ico_rew_p.png"
                    onClicked: media.prev()

                }

                AudioSwitchBtn {
                    id: playBtn
                    onIcon: "qrc:///res/img/Media/icons/media_ico_pause_p.png"
                    offIcon: "qrc:///res/img/Media/icons/media_ico_play_p.png"
                    on: media.playing
                    onSwitched: on ? media.play() : media.pause()
                }

                AudioBtn {
                    id: nextBtn
                    icon: "qrc:///res/img/Media/icons/media_ico_for_p.png"

                    onClicked: media.next()

                }

                AudioSwitchBtn {
                    id: repeatBtn
                    onIcon: "qrc:///res/img/Media/icons/ico_repeat_one_f.png"
                    offIcon: "qrc:///res/img/Media/icons/ico_repeat_all_f.png"

                    onSwitched: media.repeating = on
                }
            }




        }

        Item {
            id: progressBarWrapper
            height: 100

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Text {
                id: currentTime
                text: formatTime(media.player.position)
                font.pointSize: 28
                color: 'white'

                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                }
            }

            Slider {
                id: progressBar
                anchors.verticalCenter: parent.verticalCenter

                from: 0
                to: media.player.duration
                value: media.player.position

//                Binding {
//                    target: media.player
//                    property: 'position'
//                    value: progressBar.value
//                }



                background: Image {
                    anchors.verticalCenter: parent.verticalCenter

                    source: "qrc:///res/img/Media/backgrounds/media_pg_n.png"

                    Image {
                        anchors.verticalCenter: parent.verticalCenter
                        width: parent.width * progressBar.visualPosition
                        source: "qrc:///res/img/Media/backgrounds/media_pg_s.png"
                    }
                }

                handle: Image {
                    x: progressBar.width * progressBar.visualPosition - width / 2
                    anchors.verticalCenter: parent.verticalCenter
                    source: "qrc:///res/img/Media/backgrounds/media_pg_circle.png"
                }


                anchors {
                    verticalCenter: parent.verticalCenter

                    left: currentTime.right
                    right: totalTime.left
                }

                onValueChanged: {
                    media.player.position = value
                }
            }

            Text {
                id: totalTime
                text: formatTime(media.player.duration)
                font.pointSize: 28
                color: 'white'

                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                }
            }


        }
    }

    function formatTime(miliseconds) {
        return Qt.formatTime(new Date(miliseconds), 'mm:ss')
    }

    Connections {
        target: media
        function onCurrentIndexChanged(index) {
            audioList.currentIndex = index
            pathView.currentIndex = index
        }
    }

}
