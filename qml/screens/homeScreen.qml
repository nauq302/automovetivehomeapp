import QtQuick 2.0
import QtQml.Models 2.15
import "../components"


Item {
    width: parent.width
    height: parent.height


    ListView {
        id: widgets
        orientation: Qt.Horizontal
        interactive: false
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: parent.height / 2

        model: home.widgetModel
        delegate: Widget {
            widget: display
            appCode: 'media'
            width: widgets.width / 3 - 10
            height: parent.height

        }
    }

    ListView {
        id: menu
        orientation: Qt.Horizontal
        anchors {
            top: widgets.bottom
            left: parent.left
            right: parent.right
        }

        height: parent.height / 2

        model: home.listModel

        delegate: Widget {
            width: widgets.width / 6 - 5
            height: parent.height


            Text {
                anchors.centerIn: parent
                color: 'white'
                text: displayName
            }

            Image {
                width: parent.width
                height: parent.height
                source: 'qrc:///res/img/HomeScreen/' + icon
            }


            appCode: app
        }

    }


}
